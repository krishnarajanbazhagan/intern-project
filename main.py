import pandas as pd

CONSECUTIVE_DAYS_THRESHOLD = 7
MIN_HOURS_BETWEEN_SHIFTS = 1
MAX_HOURS_BETWEEN_SHIFTS = 10
MAX_HOURS_SINGLE_SHIFT = 14

def load_employee_data(file_path):
    try:
        df = pd.read_excel(file_path)  
        return df
    except Exception as e:
        print(f"Error loading data: {e}")
        return None

def check_consecutive_days(start_date, end_date, employee_name, position_id):
    if pd.notna(start_date) and pd.notna(end_date):
        start_date = pd.to_datetime(start_date)
        end_date = pd.to_datetime(end_date)
        days_worked = (end_date - start_date).days + 1
        if days_worked >= CONSECUTIVE_DAYS_THRESHOLD:
            return f"Employee: {employee_name}, Position ID: {position_id} has worked for {days_worked} consecutive days."
    return None

def check_hours_between_shifts(time_in, time_out, employee_name, position_id):
    if pd.notna(time_in) and pd.notna(time_out):
        hours_between_shifts = (time_out - time_in).seconds // 3600
        if MIN_HOURS_BETWEEN_SHIFTS < hours_between_shifts < MAX_HOURS_BETWEEN_SHIFTS:
            return f"Employee: {employee_name}, Position ID: {position_id} has less than 10 hours between shifts."
    return None

def check_hours_single_shift(timecard, employee_name, position_id):
    if pd.notna(timecard):
        hours_worked = int(timecard.split(':')[0])
        if hours_worked > MAX_HOURS_SINGLE_SHIFT:
            return f"Employee: {employee_name}, Position ID: {position_id} has worked for more than 14 hours."
    return None

def analyze_employee_data(file_path):
    df = load_employee_data(file_path)
    if df is None:
        return []

    results = []

    for index, row in df.iterrows():
        position_id = row['Position ID']
        employee_name = row['Employee Name']

        consecutive_days_result = check_consecutive_days(row['Pay Cycle Start Date'], row['Pay Cycle End Date'], employee_name, position_id)
        if consecutive_days_result:
            results.append(consecutive_days_result)

        hours_between_shifts_result = check_hours_between_shifts(row['Time'], row['Time Out'], employee_name, position_id)
        if hours_between_shifts_result:
            results.append(hours_between_shifts_result)

        hours_single_shift_result = check_hours_single_shift(row['Timecard Hours (as Time)'], employee_name, position_id)
        if hours_single_shift_result:
            results.append(hours_single_shift_result)

    return results

def save_to_csv(results, output_path):
    df = pd.DataFrame(results, columns=['Result'])
    df.to_csv(output_path, index=False)

if __name__ == "__main__":
    file_path = "data/Assignment_Timecard.xlsx"
    results = analyze_employee_data(file_path)
    save_to_csv(results, "results.csv")
